using EngieCodingChallenge.Business.ConcreteService;
using EngieCodingChallenge.Business.DataInterfaces;
using EngieCodingChallenge.Entities;
using EngieCodingChallenge.Entities.Enumeration;
using EngieCodingChallenge.Entities.Request;
using EngieCodingChallenge.Entities.Response;
using EngieCodingChallenge.Exceptions;
using EngieCodingChallenge.Localisations;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestPlatform.ObjectModel;
using Moq;

namespace EngieCodingChallenge.Tests.BusinessTests
{
    [Trait("Category","ProductionRepository")]
    public class ProductionRepositoryShould
    {
        private Payload payload;
        List<EnergySupplier> energySuppliers;
        private Fuels _baseEnergyMetrics;
        private List<Powerplant> powerPlants;
        private readonly ProductionService _productionServiceWithCO2;
        private readonly ProductionService _productionServiceWithoutCO2;

        public ProductionRepositoryShould()
        {
            _baseEnergyMetrics = new Fuels() { Co2EuroTon = 20, KerosineEuroMWh = 50, GasEuroMWh = 15, Wind = 50 };

            powerPlants = new List<Powerplant>()
            {
                new Powerplant("gasfiredbig1", "gasfired", 0.53, 100, 460),
                new Powerplant("gasfiredbig2", "gasfired", 0.53, 100, 460),
                new Powerplant("gasfiredsomewhatsmaller", "gasfired", 0.37, 40, 210),
                new Powerplant("tj1", "turbojet", 0.3, 0, 16),
                new Powerplant("windpark1", "windturbine", 1, 0, 150),
                new Powerplant("windpark2", "windturbine", 1, 0, 36)
            };
            payload = new Payload(480, _baseEnergyMetrics, powerPlants);

            energySuppliers = new List<EnergySupplier>
            {
                new EnergySupplier("windpark1", 75),
                new EnergySupplier("windpark2", 18),
                new EnergySupplier("gasfiredbig1", 243),
                new EnergySupplier("gasfiredbig2", 126)
            };

            Mock<IConfigurationSection> configurationSection = new();
            configurationSection.SetupGet(x => x.Value).Returns("false");
            Mock<IConfiguration> configuration = new();
            configuration.Setup(x => x.GetSection(It.IsAny<string>()))
                    .Returns(configurationSection.Object);
            Mock<IStringLocalizer<EngieChallenge>> stringLocalisation = new();
            
            _productionServiceWithoutCO2 = new ProductionService(configuration.Object, stringLocalisation.Object);

            Mock<IConfigurationSection> configurationSectionCO2Enabled = new();
            configurationSectionCO2Enabled.SetupGet(x => x.Value).Returns("true");
            Mock<IConfiguration> configurationCO2Enabled = new();
            configurationCO2Enabled.Setup(x => x.GetSection(It.IsAny<string>()))
                    .Returns(configurationSectionCO2Enabled.Object);
            _productionServiceWithCO2 = new ProductionService(configurationCO2Enabled.Object, stringLocalisation.Object);

        }


        [Fact]
        public void ReturnTheResultFromTheWindPark1AfterTheCalculation()
        {
            var calculatedSuppliedEnergy = _productionServiceWithoutCO2.CalculateSuppliedEnergy(payload);

            
            Assert.NotNull(calculatedSuppliedEnergy.FirstOrDefault(cse => cse.Name == "windpark1"));
            Assert.Equal(calculatedSuppliedEnergy.FirstOrDefault(cse => cse.Name == "windpark1")?.Name, energySuppliers.FirstOrDefault(cse => cse.Name == "windpark1")?.Name);
            Assert.Equal(calculatedSuppliedEnergy.FirstOrDefault(cse => cse.Name == "windpark1")?.SuppliedElectricity, energySuppliers.FirstOrDefault(cse => cse.Name == "windpark1")?.SuppliedElectricity);

            
        }

        [Fact]
        public void ReturnTheExactAmountOfElectricityAfterTheCalculationAndTheSmallLoad()
        {
            payload.Load = 75;
            var calculatedSuppliedEnergy = _productionServiceWithoutCO2.CalculateSuppliedEnergy(payload);

            Assert.Equal(calculatedSuppliedEnergy.FirstOrDefault(cse => cse.Name == "windpark1")?.SuppliedElectricity, payload.Load);
        }

        [Fact]
        public void ReturnNotNullWithZeroElectricitySuppliedForWindturbineWhenThereIsNoWind()
        {
            Fuels fuels = new(13.4, 50.8, 20, 0);
            payload = new Payload(400, fuels, powerPlants);

            energySuppliers = new List<EnergySupplier>
            {
                new EnergySupplier("windpark1", 0),
                new EnergySupplier("windpark2", 0),
                new EnergySupplier("gasfiredbig1", 243),
                new EnergySupplier("gasfiredbig2", 237)
            };

            var calculatedSuppliedEnergy = _productionServiceWithoutCO2.CalculateSuppliedEnergy(payload);

            Assert.NotNull(calculatedSuppliedEnergy.FirstOrDefault(cse => cse.Name == "windpark1"));

        }

        [Fact]
        public void ReturnTheResultOnlyGasAndTurboJetWhenThereIsNoWind()
        {
            Fuels fuels = new(13.4, 50.8, 20, 0);
            payload = new Payload(400, fuels, powerPlants);

            energySuppliers = new List<EnergySupplier>
            {
                new EnergySupplier("windpark1", 0),
                new EnergySupplier("windpark2", 0),
                new EnergySupplier("gasfiredbig1", 400.0),
                new EnergySupplier("gasfiredbig2", 0.0)
            };

            var calculatedSuppliedEnergy = _productionServiceWithoutCO2.CalculateSuppliedEnergy(payload);

            Assert.Equal(calculatedSuppliedEnergy.FirstOrDefault(cse => cse.Name == "gasfiredbig1")?.SuppliedElectricity, energySuppliers.FirstOrDefault(cse => cse.Name == "gasfiredbig1")?.SuppliedElectricity);
            Assert.Equal(calculatedSuppliedEnergy.FirstOrDefault(cse => cse.Name == "gasfiredbig2")?.SuppliedElectricity, energySuppliers.FirstOrDefault(cse => cse.Name == "gasfiredbig2")?.SuppliedElectricity);

        }



        [Fact]
        public void ReturnTheResultTurboJetFirstWhenGasIsMoreExpensiveThanKerosine()
        {
            Fuels fuels = new(203.4, 0, 20, 0);
            payload = new Payload(400, fuels, powerPlants);

            energySuppliers.Clear();
            energySuppliers.Add(new EnergySupplier("windpark1", 0));
            energySuppliers.Add(new EnergySupplier("windpark2", 0));
            energySuppliers.Add(new EnergySupplier("tj1", 16.0));
            energySuppliers.Add(new EnergySupplier("gasfiredbig1", 384.0));
            energySuppliers.Add(new EnergySupplier("gasfiredbig2", 0.0));


            var calculatedSuppliedEnergy = _productionServiceWithoutCO2.CalculateSuppliedEnergy(payload);

            Assert.Equal(calculatedSuppliedEnergy.FirstOrDefault(cse => cse.Name == "tj1")?.SuppliedElectricity, energySuppliers.FirstOrDefault(cse => cse.Name == "tj1")?.SuppliedElectricity);
            Assert.Equal(calculatedSuppliedEnergy.FirstOrDefault(cse => cse.Name == "gasfiredbig1")?.SuppliedElectricity, energySuppliers.FirstOrDefault(cse => cse.Name == "gasfiredbig1")?.SuppliedElectricity);
            Assert.Equal(calculatedSuppliedEnergy.FirstOrDefault(cse => cse.Name == "gasfiredbig2")?.SuppliedElectricity, energySuppliers.FirstOrDefault(cse => cse.Name == "gasfiredbig2")?.SuppliedElectricity);

        }




        [Fact]
        public void ThrowAnErrorWhenTooMuchEnergyShouldBeSupplied()
        {
            payload.Load = 5000;
            Assert.Throws<NotEnoughResourcesException>(() => _productionServiceWithoutCO2.CalculateSuppliedEnergy(payload));

        }

        [Fact]
        public void ThrowAnErrorWhenNoPowerPlantCanProduceDueToTheMinimalAmountOfElectricityToProduce()
        {
            payload.Load = 20;
            foreach(var powerPlant in payload.Powerplants)
            {
                powerPlant.Pmin = 200;
            }
            Assert.Throws<MinimumProductionNotReachedForAnyPowerPlantException>(() => _productionServiceWithoutCO2.CalculateSuppliedEnergy(payload));

        }

        [Fact]
        public void ComputeBestPowerUsage_Wind_Enough()
        {
            // arrange
            Payload productionPlan = new(25, _baseEnergyMetrics, new List<Powerplant>()
            {
                new("Gas1", PowerPlantType.gasfired.ToString(), 0.5, 10, 100),
                new("Wind1", PowerPlantType.windturbine.ToString(), 1, 0, 50)
            });

            
            // act
            var result = _productionServiceWithoutCO2.CalculateSuppliedEnergy(productionPlan).ToList();

            // assert
            Assert.Equal(25, result.First(x => x.Name == "Wind1").SuppliedElectricity);
            Assert.Equal(0, result.First(x => x.Name == "Gas1").SuppliedElectricity);
        }

        [Fact]
        public void ComputeBestPowerUsage_Wind_NotEnough()
        {
            // arrange
            Payload productionPlan = new(50, _baseEnergyMetrics, new List<Powerplant>()
            {
                new("Gas1", PowerPlantType.gasfired.ToString(), 0.5, 10, 100),
                new("Wind1", PowerPlantType.windturbine.ToString(), 1, 0, 50)
            });

            // act
            var result = _productionServiceWithoutCO2.CalculateSuppliedEnergy(productionPlan).ToList();

            // assert
            Assert.Equal(25, result.First(x => x.Name == "Wind1").SuppliedElectricity);
            Assert.Equal(25, result.First(x => x.Name == "Gas1").SuppliedElectricity);
        }

        [Fact]
        public void ComputeBestPowerUsage_Wind_TooMuch()
        {
            // arrange
            Payload productionPlan = new(20, _baseEnergyMetrics, new List<Powerplant>()
            {
                new("Gas1", PowerPlantType.gasfired.ToString(), 0.5, 10, 100),
                new("Wind1", PowerPlantType.windturbine.ToString(), 1, 0, 50)
            });

            
            // act
            var result = _productionServiceWithoutCO2.CalculateSuppliedEnergy(productionPlan).ToList();

            // assert
            Assert.Equal(0, result.First(x => x.Name == "Wind1").SuppliedElectricity);
            Assert.Equal(20, result.First(x => x.Name == "Gas1").SuppliedElectricity);
        }

        [Fact]
        public void ComputeBestPowerUsage_Gas_Efficiency()
        {
            // arrange
            Payload productionPlan = new(20, _baseEnergyMetrics, new List<Powerplant>()
            {
                new("Gas1", PowerPlantType.gasfired.ToString(), 0.5, 10, 100),
                new("Gas2", PowerPlantType.gasfired.ToString(), 0.6, 10, 100),
                new("Gas3", PowerPlantType.gasfired.ToString(), 0.8, 10, 100),
                new("Gas4", PowerPlantType.gasfired.ToString(), 0.3, 10, 100),
                new("Gas5", PowerPlantType.gasfired.ToString(), 0.45, 10, 100)
            });

            
            // act
            var result = _productionServiceWithoutCO2.CalculateSuppliedEnergy(productionPlan).ToList();

            // assert
            Assert.Equal(20, result.First(x => x.Name == "Gas3").SuppliedElectricity);
            Assert.Equal(0, result.Where(x => x.Name != "Gas3").Select(x => x.SuppliedElectricity).Sum());
        }

        [Fact]
        public void ComputeBestPowerUsage_Gas_AllNeeded()
        {
            // arrange
            Payload productionPlan = new(490, _baseEnergyMetrics, new List<Powerplant>()
            {
                new("Gas1", PowerPlantType.gasfired.ToString(), 0.5, 10, 100),
                new("Gas2", PowerPlantType.gasfired.ToString(), 0.6, 10, 100),
                new("Gas3", PowerPlantType.gasfired.ToString(), 0.8, 10, 100),
                new("Gas4", PowerPlantType.gasfired.ToString(), 0.3, 10, 100),
                new("Gas5", PowerPlantType.gasfired.ToString(), 0.45, 10, 100)
            });

            
            // act
            var result = _productionServiceWithoutCO2.CalculateSuppliedEnergy(productionPlan).ToList();

            // assert
            Assert.Equal(100, result.First(x => x.Name == "Gas1").SuppliedElectricity);
            Assert.Equal(100, result.First(x => x.Name == "Gas2").SuppliedElectricity);
            Assert.Equal(100, result.First(x => x.Name == "Gas3").SuppliedElectricity);
            Assert.Equal(90, result.First(x => x.Name == "Gas4").SuppliedElectricity);
            Assert.Equal(100, result.First(x => x.Name == "Gas5").SuppliedElectricity);
        }

        [Fact]
        public void ComputeBestPowerUsage_Gas_Pmin()
        {
            // arrange
            Payload productionPlan = new(125, _baseEnergyMetrics, new List<Powerplant>()
            {
                new("Wind1", PowerPlantType.windturbine.ToString(), 1, 0, 50),
                new("Gas1", PowerPlantType.gasfired.ToString(), 0.5, 110, 200),
                new("Gas2", PowerPlantType.gasfired.ToString(), 0.8, 80, 150)
            });

            
            // act
            var result = _productionServiceWithoutCO2.CalculateSuppliedEnergy(productionPlan).ToList();

            // assert
            Assert.Equal(100, result.First(x => x.Name == "Gas2").SuppliedElectricity);
            Assert.Equal(0, result.First(x => x.Name == "Gas1").SuppliedElectricity);
        }

        [Fact]
        public void ComputeBestPowerUsage_Kerosine()
        {
            // arrange
            Payload productionPlan = new(100, _baseEnergyMetrics, new List<Powerplant>()
            {
                new("Wind1", PowerPlantType.windturbine.ToString(), 1, 0, 150),
                new("Gas1", PowerPlantType.gasfired.ToString(), 0.5, 100, 200),
                new("Kerosine1", PowerPlantType.turbojet.ToString(), 0.5, 0, 200)
            });

            
            // act
            var result = _productionServiceWithCO2.CalculateSuppliedEnergy(productionPlan).ToList();

            // assert
            Assert.Equal(75, result.First(x => x.Name == "Wind1").SuppliedElectricity);
            Assert.Equal(0, result.First(x => x.Name == "Gas1").SuppliedElectricity);
            Assert.Equal(25, result.First(x => x.Name == "Kerosine1").SuppliedElectricity);
        }

        [Fact]
        public void ComputeBestPowerUsage_CO2Impact()
        {
            // arrange
            Payload productionPlan = new(150, _baseEnergyMetrics, new List<Powerplant>()
            {
                new("Gas1", PowerPlantType.gasfired.ToString(), 0.3, 100, 200),
                new("Kerosine1", PowerPlantType.turbojet.ToString(), 1, 0, 200)
            });


            // act
            var resultCO2 = _productionServiceWithCO2.CalculateSuppliedEnergy(productionPlan);
            var resultNoCO2 = _productionServiceWithoutCO2.CalculateSuppliedEnergy(productionPlan);
            
            // assert
            Assert.Equal(150, resultNoCO2.First(x => x.Name == "Gas1").SuppliedElectricity);
            Assert.Equal(150, resultCO2.First(x => x.Name == "Kerosine1").SuppliedElectricity);
        }

        [Fact]
        public void ComputeBestPowerUsage_TrickyTest1()
        {
            // arrange
            _baseEnergyMetrics = new() { Co2EuroTon = 0, KerosineEuroMWh = 50.8, GasEuroMWh = 20, Wind = 100 };
            Payload productionPlan = new(60, _baseEnergyMetrics, new List<Powerplant> {
                new("windpark1", PowerPlantType.windturbine.ToString(), 1, 0, 20),
                new("gasfired", PowerPlantType.gasfired.ToString(), 0.9, 50, 100),
                new("gasfiredinefficient", PowerPlantType.gasfired.ToString(), 0.1, 0, 100)
            });

            
            // act
            var result = _productionServiceWithoutCO2.CalculateSuppliedEnergy(productionPlan);

            // assert
            Assert.Equal(60, result.Select(x => x.SuppliedElectricity).Sum());
            Assert.Equal(0, result.First(x => x.Name == "windpark1").SuppliedElectricity);
            Assert.Equal(60, result.First(x => x.Name == "gasfired").SuppliedElectricity);
            Assert.Equal(0, result.First(x => x.Name == "gasfiredinefficient").SuppliedElectricity);
        }

        [Fact]
        public void ComputeBestPowerUsage_TrickyTest2()
        {
            // arrange
            _baseEnergyMetrics = new() { Co2EuroTon = 0,KerosineEuroMWh = 50.8, GasEuroMWh= 20, Wind= 100 };
            Payload productionPlan = new(80, _baseEnergyMetrics, new List<Powerplant> {
                new("windpark1", PowerPlantType.windturbine.ToString(), 1, 0, 60),
                new("gasfired", PowerPlantType.gasfired.ToString(), 0.9, 50, 100),
                new("gasfiredinefficient", PowerPlantType.gasfired.ToString(), 0.1, 0, 200)
            });

            // act
            var result = _productionServiceWithoutCO2.CalculateSuppliedEnergy(productionPlan);

            // assert
            Assert.Equal(80, result.Select(x => x.SuppliedElectricity).Sum());
            Assert.Equal(0, result.First(x => x.Name == "windpark1").SuppliedElectricity);
            Assert.Equal(80, result.First(x => x.Name == "gasfired").SuppliedElectricity);
            Assert.Equal(0, result.First(x => x.Name == "gasfiredinefficient").SuppliedElectricity);
        }

        [Fact]
        public void ComputeBestPowerUsage_ExamplePayload1_NoCO2()
        {
            // arrange
            _baseEnergyMetrics = new() { Co2EuroTon = 0, KerosineEuroMWh  = 50.8, GasEuroMWh = 13.4, Wind = 60 };
            Payload productionPlan = new(480, _baseEnergyMetrics, new List<Powerplant> {
                new("gasfiredbig1", PowerPlantType.gasfired.ToString(), 0.53, 100, 460),
                new("gasfiredbig2", PowerPlantType.gasfired.ToString(), 0.53, 100, 460),
                new("gasfiredsomewhatsmaller", PowerPlantType.gasfired.ToString(), 0.37, 40, 210),
                new("tj1", PowerPlantType.turbojet.ToString(), 0.3, 0, 16),
                new("windpark1", PowerPlantType.windturbine.ToString(), 1, 0, 150),
                new("windpark2", PowerPlantType.windturbine.ToString(), 1, 0, 36)
            });

            // act
            var result = _productionServiceWithoutCO2.CalculateSuppliedEnergy(productionPlan).ToList();

            // assert
            Assert.Equal(480, result.Select(x => x.SuppliedElectricity).Sum());
            Assert.Equal(90, result.First(x => x.Name == "windpark1").SuppliedElectricity);
            Assert.Equal(21.6, result.First(x => x.Name == "windpark2").SuppliedElectricity);
            Assert.Equal(368.4, result.First(x => x.Name == "gasfiredbig1").SuppliedElectricity);
            Assert.Equal(0, result.First(x => x.Name == "gasfiredbig2").SuppliedElectricity);
            Assert.Equal(0, result.First(x => x.Name == "gasfiredsomewhatsmaller").SuppliedElectricity);
            Assert.Equal(0, result.First(x => x.Name == "tj1").SuppliedElectricity);
        }

        [Fact]
        public void ComputeBestPowerUsage_ExamplePayload2_NoCO2()
        {
            // arrange
            _baseEnergyMetrics = new() { Co2EuroTon = 0, KerosineEuroMWh = 50.8, GasEuroMWh  = 13.4, Wind = 0 };
            Payload productionPlan = new(480, _baseEnergyMetrics, new List<Powerplant> {
                new("gasfiredbig1", PowerPlantType.gasfired.ToString(), 0.53, 100, 460),
                new("gasfiredbig2", PowerPlantType.gasfired.ToString(), 0.53, 100, 460),
                new("gasfiredsomewhatsmaller", PowerPlantType.gasfired.ToString(), 0.37, 40, 210),
                new("tj1", PowerPlantType.turbojet.ToString(), 0.3, 0, 16),
                new("windpark1", PowerPlantType.windturbine.ToString(), 1, 0, 150),
                new("windpark2", PowerPlantType.windturbine.ToString(), 1, 0, 36)
            });

            // act
            var result = _productionServiceWithoutCO2.CalculateSuppliedEnergy(productionPlan).ToList();

            // assert
            Assert.Equal(480, result.Select(x => x.SuppliedElectricity).Sum());
            Assert.Equal(0, result.First(x => x.Name == "windpark1").SuppliedElectricity);
            Assert.Equal(0, result.First(x => x.Name == "windpark2").SuppliedElectricity);
            Assert.Equal(380, result.First(x => x.Name == "gasfiredbig1").SuppliedElectricity);
            Assert.Equal(100, result.First(x => x.Name == "gasfiredbig2").SuppliedElectricity);
            Assert.Equal(0, result.First(x => x.Name == "gasfiredsomewhatsmaller").SuppliedElectricity);
            Assert.Equal(0, result.First(x => x.Name == "tj1").SuppliedElectricity);
        }

        [Fact]
        public void ComputeBestPowerUsage_ExamplePayload3_NoCO2()
        {
            // arrange
            _baseEnergyMetrics = new() { Co2EuroTon= 0, KerosineEuroMWh = 50.8, GasEuroMWh = 13.4, Wind = 60 };
            Payload productionPlan = new(910, _baseEnergyMetrics, new List<Powerplant> {
                new("gasfiredbig1", PowerPlantType.gasfired.ToString(), 0.53, 100, 460),
                new("gasfiredbig2", PowerPlantType.gasfired.ToString(), 0.53, 100, 460),
                new("gasfiredsomewhatsmaller", PowerPlantType.gasfired.ToString(), 0.37, 40, 210),
                new("tj1", PowerPlantType.turbojet.ToString(), 0.3, 0, 16),
                new("windpark1", PowerPlantType.windturbine.ToString(), 1, 0, 150),
                new("windpark2", PowerPlantType.windturbine.ToString(), 1, 0, 36)
            });

            // act
            var result = _productionServiceWithoutCO2.CalculateSuppliedEnergy(productionPlan).ToList();

            // assert
            Assert.Equal(910, result.Select(x => x.SuppliedElectricity).Sum());
            Assert.Equal(90, result.First(x => x.Name == "windpark1").SuppliedElectricity);
            Assert.Equal(21.6, result.First(x => x.Name == "windpark2").SuppliedElectricity);
            Assert.Equal(460, result.First(x => x.Name == "gasfiredbig1").SuppliedElectricity);
            Assert.Equal(338.4, result.First(x => x.Name == "gasfiredbig2").SuppliedElectricity);
            Assert.Equal(0, result.First(x => x.Name == "gasfiredsomewhatsmaller").SuppliedElectricity);
            Assert.Equal(0, result.First(x => x.Name == "tj1").SuppliedElectricity);
        }
    }
}
