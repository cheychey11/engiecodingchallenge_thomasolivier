﻿namespace EngieCodingChallenge.Exceptions
{
    public class MinimumProductionNotReachedForAnyPowerPlantException : Exception
    {
        public MinimumProductionNotReachedForAnyPowerPlantException() { }

        public MinimumProductionNotReachedForAnyPowerPlantException(string message)
            : base(message) { }

        public MinimumProductionNotReachedForAnyPowerPlantException(string message, Exception inner)
            : base(message, inner) { }
    }
}
