using EngieCodingChallenge.Business.ConcreteService;
using EngieCodingChallenge.Business.DataInterfaces;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddLocalization();
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo
    {
        Title = "Engie Coding Challenge Api",
        Version = "v" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version,
        Description = "An Api to generate a production plan",
    });
});

builder.Services.AddScoped<IProductionService, ProductionService>();

var app = builder.Build();

var supportedCultures = new[] { "en", "fr" };
var localizationOptions =
    new RequestLocalizationOptions().SetDefaultCulture(supportedCultures[0])
    .AddSupportedCultures(supportedCultures)
    .AddSupportedUICultures(supportedCultures);

app.UseRequestLocalization(localizationOptions);

//Here the cors is opened to any origin, any header, any method
// Should be restricted to the origin (domain name that can access the API) 
// Should be restricted to the header (example : Content-Type, x-requested-with)
// Should be restricted to the method (example : POST, GET, OPTIONS, DELETE)
app.UseCors(options => 
    options
    .AllowAnyOrigin()
    .AllowAnyHeader()
    .AllowAnyMethod()
);

//Activate for the coding test -> Should be only accessible in dev 
app.UseSwagger();
app.UseSwaggerUI(options =>
{
    options.SwaggerEndpoint("/swagger/v1/swagger.json", "V1");
});



app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
