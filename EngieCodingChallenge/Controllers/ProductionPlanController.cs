﻿using EngieCodingChallenge.Business.ConcreteService;
using EngieCodingChallenge.Business.DataInterfaces;
using EngieCodingChallenge.Entities;
using EngieCodingChallenge.Entities.Request;
using EngieCodingChallenge.Entities.Response;
using EngieCodingChallenge.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.ComponentModel;
using System.Net;
using System.Text.Json;

namespace EngieCodingChallenge.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductionPlanController : ControllerBase
    {
        private readonly IProductionService _productionService;

        public ProductionPlanController(IProductionService productionService)
        {
            _productionService = productionService; 
        }

        [HttpPost(Name = "productionplan")]
        [Produces("application/json")]
        public IActionResult Productionplan([FromBody] Payload payload)
        {
            try
            {
                return Ok(_productionService.CalculateSuppliedEnergy(payload));
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }  
        }
    }
}
