﻿using EngieCodingChallenge.Entities;
using EngieCodingChallenge.Entities.Response;

namespace EngieCodingChallenge.Business.DataInterfaces
{
    public interface IProductionService
    {
        public List<EnergySupplier> CalculateSuppliedEnergy(Payload payload);
    }
}
