﻿using EngieCodingChallenge.Business.DataInterfaces;
using EngieCodingChallenge.Entities;
using EngieCodingChallenge.Entities.Enumeration;
using EngieCodingChallenge.Entities.Response;
using EngieCodingChallenge.Exceptions;
using EngieCodingChallenge.Localisations;
using Microsoft.Extensions.Localization;
using System.IO.Pipes;

namespace EngieCodingChallenge.Business.ConcreteService
{
    public class ProductionService : IProductionService
    {
        const double CST_PRODUCTIONOFCO2BYMWH = 0.3;

        private readonly IStringLocalizer<EngieChallenge> _stringLocalizer;

        private readonly IConfiguration _configuration;

        public ProductionService(IConfiguration configuration, IStringLocalizer<EngieChallenge> stringLocalizer)
        {
            _configuration = configuration;
            _stringLocalizer = stringLocalizer;
        }


        /// <summary>
        /// Calculate the suppliedEnergy for each powerplant and retrieve a production plan 
        /// </summary>
        /// <param name="payload">Payload from the request that contains a list of powerplants, a load and the fuels (cost for gas, kerosine, CO2/Ton and the power of the wind) </param>
        /// <returns>A list of Energy supplier with the name of the power plants and the power provided by each of the power plant</returns>
        /// <exception cref="MinimumProductionNotReachedForAnyPowerPlantException">Thrown when the Pmin cannot be reached because of minimum production or/and constraints on PowerPlant</exception>
        /// <exception cref="NotEnoughResourcesException">Thrown when the load is higher than the sum of maximum production of all powerplants</exception>
        public List<EnergySupplier> CalculateSuppliedEnergy(Payload payload)
        {
            bool co2Enabled = bool.Parse(_configuration.GetSection("EnableCO2").Value);

            var loadLeft = payload.Load;
            List<EnergySupplier> energySuppliers = new();
            EnergySupplier energySupplier = new("", 0);

            var powerPlants = SortPowerPlantByMeritOrder(payload, co2Enabled);

            if (powerPlants.All(pp => (pp.Pmin > loadLeft)))
                throw new MinimumProductionNotReachedForAnyPowerPlantException(_stringLocalizer.GetString("PminNotReachedAllPowerPlant"));
            
            for(var i = 0; i< powerPlants.Count; i++)
            {
                int indexMax = powerPlants.Count - 1;
                var currentPowerPlant = powerPlants[i];
                Powerplant? nextPowerPlant = null;
                if (i != indexMax)
                    nextPowerPlant = powerPlants[i + 1];

                if ((currentPowerPlant.Pmin * currentPowerPlant.Efficiency) > loadLeft)
                {
                    energySuppliers.Add(new EnergySupplier(currentPowerPlant.Name, 0));
                    continue;
                }
                switch (currentPowerPlant.Type)
                {
                    case "windturbine":
                        energySupplier = CalculateEnergy(PowerPlantType.windturbine, loadLeft, currentPowerPlant, nextPowerPlant, payload.Fuels.Wind);
                        break;
                    case "gasfired":
                        energySupplier = CalculateEnergy(PowerPlantType.gasfired, loadLeft, currentPowerPlant, nextPowerPlant, null);
                        break;
                    case "turbojet":
                        energySupplier = CalculateEnergy(PowerPlantType.turbojet, loadLeft, currentPowerPlant, nextPowerPlant, null);
                        break;
                }
                if (loadLeft < energySupplier.SuppliedElectricity)
                    energySupplier.SuppliedElectricity = Math.Round(loadLeft,1);
                
                energySuppliers.Add(energySupplier);
                
                loadLeft -= energySupplier.SuppliedElectricity;
            }

            if (energySuppliers.Count != powerPlants.Count && loadLeft > 0)
                throw new MinimumProductionNotReachedForAnyPowerPlantException(_stringLocalizer.GetString("PminNotReached"));

            if (loadLeft > 0)
                throw new NotEnoughResourcesException(_stringLocalizer.GetString("NotEnoughResources"));

            return energySuppliers;
        }

        /// <summary>
        /// This method will make a calculation to know how much power will be produced by a particular powerplant
        /// </summary>
        /// <param name="powerPlantType">(Enumeration PowerPlantType) Type of the powerPlant</param>
        /// <param name="loadLeft">(double) The load left to produce for </param>
        /// <param name="powerplant">(PowerPlant) A complete powerplant with all the parameters</param>
        /// <param name="nextPowerPlant">(PowerPlant) (optional) The next powerplant that will be taken in account in the calculation</param>
        /// <param name="windpower">(int) (optional) The power of the wind for the calculation of the windturbine </param>
        /// <returns>EnergySupplier that contains the name of the powerplant and the production generated </returns>
        private static EnergySupplier CalculateEnergy(PowerPlantType powerPlantType, double loadLeft, Powerplant powerplant, Powerplant? nextPowerPlant,  int? windpower)
        {
            EnergySupplier energyGenerated = new(powerplant.Name, 0);
            var energyMaxToProduce = (double)powerplant.Pmax;

            energyMaxToProduce = loadLeft < energyMaxToProduce ? loadLeft : energyMaxToProduce;

            if (CalculateIfNextPowerPlantsCanHandleTheNeedOfElectricity(powerplant, nextPowerPlant, loadLeft, windpower))
                energyMaxToProduce = CalculateMaxOfCurrentForProducingWithTheNextPowerPlant(powerplant, nextPowerPlant, loadLeft, windpower);

            if (loadLeft < powerplant.Pmin)
                return energyGenerated;
            else if (powerPlantType.Equals(PowerPlantType.windturbine) && windpower != null)
            {
                double energyMaxToProduceByWindTurbine = powerplant.Pmax * powerplant.Efficiency * (windpower / 100.0) ?? 0;
                if (loadLeft < energyMaxToProduceByWindTurbine)
                {
                    energyGenerated.SuppliedElectricity = 0;
                    energyGenerated.Name = powerplant.Name;
                }
                else
                {
                    energyGenerated.SuppliedElectricity = energyMaxToProduceByWindTurbine > energyMaxToProduce ? energyMaxToProduce : energyMaxToProduceByWindTurbine;
                    energyGenerated.Name = powerplant.Name;
                }
            }
            else if (powerPlantType.Equals(PowerPlantType.turbojet) || powerPlantType.Equals(PowerPlantType.gasfired))
            {
                energyGenerated.SuppliedElectricity = energyMaxToProduce;
                energyGenerated.Name = powerplant.Name;

            }

            energyGenerated.SuppliedElectricity = Math.Round(energyGenerated.SuppliedElectricity, 1);

            return energyGenerated;
        }

        /// <summary>
        /// Sort the powerplant to use in a particular order taken in account (the cost and the efficiency)
        /// </summary>
        /// <param name="payload">Payload that contains a list with all type of powerplants </param>
        /// <param name="enableGasCalculation">Parameters to know if the cost of the CO2 price has to be taken in account</param>
        /// <returns>The list of powerplant in a particular order</returns>
        private static List<Powerplant> SortPowerPlantByMeritOrder(Payload payload, bool enableGasCalculation)
        {
            var listPowerPlantsByMeritOrder = new List<Powerplant>();
            var powerPlantWindTurbine = PowerPlantWindTurbine(payload);
            var powerPlantGases = PowerPlantGases(payload);
            var powerPlantTurboJets = PowerPlanTurboJets(payload);

            listPowerPlantsByMeritOrder.AddRange(powerPlantWindTurbine);
            
            foreach (var powerPlant in powerPlantGases)
            {
                var requiredForOneMW = powerPlant.Efficiency != 0 ? 1 / powerPlant.Efficiency : Double.MaxValue;
                var priceGasForOneMW = requiredForOneMW * payload.Fuels.GasEuroMWh;
                if(enableGasCalculation)
                    priceGasForOneMW += (payload.Fuels.Co2EuroTon*CST_PRODUCTIONOFCO2BYMWH);

                foreach (var powerplantTurboJet in powerPlantTurboJets)
                {
                    var fuelrequiredForOneMW = 1 / powerplantTurboJet.Efficiency;
                    var priceFuelForOneMW = fuelrequiredForOneMW * payload.Fuels.KerosineEuroMWh;

                    if (priceFuelForOneMW < priceGasForOneMW 
                        && !listPowerPlantsByMeritOrder.Contains(powerplantTurboJet))
                        listPowerPlantsByMeritOrder.Add(powerplantTurboJet);
                }
                listPowerPlantsByMeritOrder.Add(powerPlant);
            }

            foreach (var powerplantTurboJet in powerPlantTurboJets)
            {
                if (!listPowerPlantsByMeritOrder.Contains(powerplantTurboJet))
                    listPowerPlantsByMeritOrder.Add(powerplantTurboJet);
            }

            return listPowerPlantsByMeritOrder;
        }

        /// <summary>
        /// This method is calculating if it is necessary to reduce the production of the current power plant to reach the minimal production of the next power plant
        /// </summary>
        /// <param name="currentPowerPlant">(PowerPlant) The powerplant for the current index </param>
        /// <param name="nextPowerPlant">(PowerPlant) The powerplant for the next index </param>
        /// <param name="loadLeft">(double) The load that still needs to be produced </param>
        /// <param name="windpower">(int) (optional) The power of the wind, required for windturbine</param>
        /// <returns>boolean if it is necessary to split the production with the two power plant </returns>
        private static bool CalculateIfNextPowerPlantsCanHandleTheNeedOfElectricity(Powerplant currentPowerPlant, Powerplant? nextPowerPlant, double loadLeft, int? windpower)
        {
            if (nextPowerPlant == null)
                return false;

            var pmax = currentPowerPlant.Type==PowerPlantType.windturbine.ToString() ? currentPowerPlant.Pmax * currentPowerPlant.Efficiency * (windpower/100.0) : currentPowerPlant.Pmax;
            return (loadLeft > pmax
                && ((loadLeft - pmax) < nextPowerPlant.Pmin));
        }

        /// <summary>
        /// This method is calculating the maximum production for the current to allow the next powerplant to produce the minimal production required
        /// </summary>
        /// <param name="currentPowerPlant">(PowerPlant) The powerplant for the current index </param>
        /// <param name="nextPowerPlant">(PowerPlant) The powerplant for the next index </param>
        /// <param name="loadLeft">(double) The load that still needs to be produced </param>
        /// <param name="windpower">(int) (optional) The power of the wind, required for windturbine</param>
        /// <returns>(double)The Maximum power that can be produced by the current powerplant</returns>
        private static double CalculateMaxOfCurrentForProducingWithTheNextPowerPlant(Powerplant currentPowerPlant, Powerplant? nextPowerPlant,  double loadLeft, int? windpower)
        {

            if (nextPowerPlant?.Pmax > loadLeft 
                && currentPowerPlant.Type== PowerPlantType.windturbine.ToString()
                && nextPowerPlant.Efficiency > 0.5)
                return 0.0;


            if (currentPowerPlant.Type.Equals(PowerPlantType.windturbine.ToString()))
            {
                return currentPowerPlant.Pmax * currentPowerPlant.Efficiency * ((windpower ?? 0.0) / 100.0) * 1.0;
            }
            else
            {
                var pminNextPowerPlant = nextPowerPlant == null ? 0.0 : nextPowerPlant.Pmin;
                return loadLeft - pminNextPowerPlant;
            }
        }

        /// <summary>
        /// A method to return all the powerplant with the type gasfired
        /// </summary>
        /// <param name="payload">Payload that contains a list with all type of powerplants </param>
        /// <returns>A list of powerplant of Gasfired</returns>
        private static List<Powerplant> PowerPlantGases(Payload payload)
        {
            return payload.Powerplants
                        .Where(p => p.Type == PowerPlantType.gasfired.ToString())
                        .OrderByDescending(p => p.Efficiency)
                        .ThenBy(p => p.Pmin)
                        .ToList();
        }

        /// <summary>
        /// A method to return all the powerplant with the type windturbine
        /// </summary>
        /// <param name="payload">Payload that contains a list with all type of powerplants </param>
        /// <returns>A list of powerplant of Windturbine</returns>
        private static List<Powerplant> PowerPlantWindTurbine(Payload payload)
        {
            return payload
                        .Powerplants
                        .Where(p => p.Type == PowerPlantType.windturbine.ToString())
                        .OrderByDescending(p => p.Efficiency)
                        .ThenBy(p => p.Pmin)
                        .ToList();
        }

        /// <summary>
        /// A method to return all the powerplant with the type TurboJets
        /// </summary>
        /// <param name="payload">Payload that contains a list with all type of powerplants </param>
        /// <returns>A list of powerplant of TurboJets</returns>
        private static List<Powerplant> PowerPlanTurboJets(Payload payload)
        {
            return payload.Powerplants
                        .Where(p => p.Type == PowerPlantType.turbojet.ToString())
                        .OrderByDescending(p => p.Efficiency)
                        .ThenBy(p => p.Pmin)
                        .ToList();
        }
    }
}
