﻿using Newtonsoft.Json;
using System.Text.Json.Serialization;

namespace EngieCodingChallenge.Entities.Response
{
    public class EnergySupplier
    {
        public EnergySupplier(string name, double suppliedElectricity)
        {
            Name = name;
            SuppliedElectricity = suppliedElectricity;
        }

        [JsonPropertyName("name")]
        public string Name { get; set; }
        
        [JsonPropertyName("p")]
        public double SuppliedElectricity { get; set; }
    }
}
