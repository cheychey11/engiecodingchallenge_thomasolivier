﻿using System.Runtime.Serialization;

namespace EngieCodingChallenge.Entities.Enumeration
{
    public enum PowerPlantType
    {
        [EnumMember(Value="gasfired")]
        gasfired,
        [EnumMember(Value = "turbojet")]
        turbojet,
        [EnumMember(Value = "windturbine")]
        windturbine
    }
}
