﻿using Newtonsoft.Json;
using System.Text.Json.Serialization;

namespace EngieCodingChallenge.Entities
{
    public class Powerplant
    {
        public Powerplant(string name, string type, double efficiency, int pmin, int pmax)
        {
            Name = name;
            Type = type;
            Efficiency = efficiency;
            Pmin = pmin;
            Pmax = pmax;
        }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("type")]
        public string Type { get; set; }

        [JsonPropertyName("efficiency")]
        public double Efficiency { get; set; }

        [JsonPropertyName("pmin")]
        public int Pmin { get; set; }

        [JsonPropertyName("pmax")]
        public int Pmax { get; set; }
    }
}
