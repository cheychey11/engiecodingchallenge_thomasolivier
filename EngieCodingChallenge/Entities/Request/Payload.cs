﻿using EngieCodingChallenge.Entities.Request;
using Newtonsoft.Json;
using System.Text.Json.Serialization;

namespace EngieCodingChallenge.Entities
{
    public class Payload
    {
        public Payload() 
        {
            Powerplants = new List<Powerplant>();
            Load = 0.0;
            Fuels = new Fuels();
        }
        public Payload(int load, Fuels fuels, List<Powerplant> powerplants)
        {
            Load = load;
            Fuels = fuels;
            Powerplants = powerplants;
        }

        [JsonPropertyName("load")]
        public double Load { get; set; }

        [JsonPropertyName("fuels")]
        public Fuels Fuels { get; set; }

        [JsonPropertyName("powerplants")]
        public List<Powerplant> Powerplants { get; set; }
    }
}
