﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using System.Xml.Linq;

namespace EngieCodingChallenge.Entities.Request
{
    [DataContract]
    public class Fuels
    {
        public Fuels()
        {

        }
        public Fuels(double gasEuroMWh, double kerosineEuroMWh, int co2EuroTon, int wind)
        {
            GasEuroMWh = gasEuroMWh;
            KerosineEuroMWh = kerosineEuroMWh;
            Co2EuroTon = co2EuroTon;
            Wind = wind;
        }

        [JsonPropertyName("gas(euro/MWh)")]
        public double GasEuroMWh { get; set; }

        [JsonPropertyName("kerosine(euro/MWh)")]
        public double KerosineEuroMWh { get; set; }

        [JsonPropertyName("co2(euro/ton)")]
        public int Co2EuroTon { get; set; }

        [JsonPropertyName("wind(%)")]
        public int Wind { get; set; }
    }
}
